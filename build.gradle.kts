buildscript {
    repositories {
        gradlePluginPortal()
    }
}

plugins {
    `java-library`
}

version = "0.0.1"

allprojects {

    group = "dev.burak.byoinker"

    apply<JavaPlugin>()
    apply(plugin = "java-library")

    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }

    dependencies {
		implementation(files("../libs/rspeer.jar"))
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_11
    }

    tasks {
        build {
            doFirst {
                clean
            }
        }

        withType<Jar> {
            doLast {
                copy {
                    from("build/libs")
                    into(System.getProperty("user.home") + "/Documents/RSPeer/scripts/")
                }
            }
        }
    }
}
