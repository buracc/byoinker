package script;

public class BotAccount {
    private String name;
    private int world;

    public BotAccount(String name, int world) {
        this.name = name;
        this.world = world;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWorld() {
        return this.world;
    }

    public void setWorld(int world) {
        this.world = world;
    }
}
