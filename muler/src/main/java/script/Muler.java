//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Trade;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.ChatMessageListener;
import org.rspeer.runetek.event.types.ChatMessageEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

@ScriptMeta(
        name = "acc-looter-muler",
        developer = "burak",
        desc = ""
)
public class Muler extends Script implements ChatMessageListener {
    private BotAccount acc;
    private final File dirPath = new File(Script.getDataDirectory() + "/acclooter/");
    private boolean trade;

    public int loop() {
        if (!Inventory.isEmpty()) {
            if (!Bank.isOpen()) {
                Bank.open();
                Log.info("opening bank");
                return 1000;
            }

            Log.info("deposit inv");
            Bank.depositInventory();
            return 1000;
        }

        if (Bank.isOpen()) {
            Log.info("closing bank");
            Bank.close();
            return 1000;
        }

        if (acc != null) {
            if (Worlds.getCurrent() != acc.getWorld()) {
                if (Dialog.canContinue()) {
                    Dialog.processContinue();
                    return 1000;
                }

                WorldHopper.hopTo(acc.getWorld());
                Log.info("hopping");
                Time.sleepUntil(() -> Game.getState() == 45, 2000L);
                return 1000;
            }

            Player bot = Players.getNearest(acc.getName());
            if (bot != null) {
                if (Trade.isOpen()) {
                    Log.info("accepting trade");
                    Trade.accept();
                    this.trade = false;
                    return 1000;
                }

                if (trade) {
                    Log.info("accepting trade");
                    bot.interact("Trade with");
                }

                return 1500;
            }

            Log.info("Couldn't find player with name: " + this.acc.getName());
            return 1000;
        }

        if (this.dirPath.list() == null) {
            Log.info("idle");
            return 1000;
        }

        Optional<String> accString = Arrays.stream(dirPath.list()).findAny();
        if (accString.isPresent() && (accString.get()).toLowerCase().contains(Players.getLocal().getName().toLowerCase())) {
            String[] accc = (accString.get()).split("%");
            this.acc = new BotAccount(accc[0], Integer.parseInt(accc[1]));
            Log.info("setting new acc: " + this.acc);
        }

        return 1000;
    }

    public void onStop() {
        if (this.dirPath.list() != null) {
            File[] var1 = this.dirPath.listFiles();

            for (File file : var1) {
                if (!file.isDirectory()) {
                    file.delete();
                }
            }
        }
    }

    public void notify(ChatMessageEvent e) {
        if (e.getMessage().toLowerCase().contains("wishes to trade with you.")) {
            trade = true;
        }

        if (e.getMessage().toLowerCase().contains("accepted") || e.getMessage().toLowerCase().contains("declined")) {
            acc = null;
            File[] var2 = Objects.requireNonNull(dirPath.listFiles());

            for (File file : var2) {
                if (!file.isDirectory()) {
                    file.delete();
                }
            }
        }
    }
}
