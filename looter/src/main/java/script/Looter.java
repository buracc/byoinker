//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Collectors;
import javax.swing.SwingUtilities;

import org.rspeer.RSPeer;
import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Definitions;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.AccountType;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.EnterInput;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.Trade;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.component.Bank.WithdrawMode;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Minigames;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.component.tab.Minigames.Destination;
import org.rspeer.runetek.api.component.tab.Spell.Modern;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.runetek.api.input.Mouse;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.query.results.InterfaceComponentQueryResults;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ChatMessageListener;
import org.rspeer.runetek.event.listeners.LoginResponseListener;
import org.rspeer.runetek.event.types.ChatMessageEvent;
import org.rspeer.runetek.event.types.LoginResponseEvent;
import org.rspeer.runetek.providers.RSWorld;
import org.rspeer.runetek.providers.subclass.GameCanvas;
import org.rspeer.script.GameAccount;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.events.LoginScreen;
import org.rspeer.ui.Log;
import util.ExWilderness;

@ScriptMeta(
        desc = "",
        developer = "burak",
        name = "acc-looter",
        version = 1.4
)
public class Looter extends Script implements LoginResponseListener, ChatMessageListener {
    private Path txtPath = Paths.get(Script.getDataDirectory() + "/accounts.txt");
    private Path out = Paths.get(Script.getDataDirectory() + "/checked.txt");
    private Stack<Acc> loadedAccs = new Stack<>();
    private List<Acc> checkedAccs = new ArrayList<>();
    private GameAccount account = null;
    private Acc acc = null;
    private List<Integer> f2p = List.of(301, 316, 335, 326, 379, 380, 382, 383, 384, 398, 399, 417, 418, 425, 426, 430, 431, 433, 434, 500, 501, 502, 503, 504);
    private String response = null;
    private Area cw = Area.rectangular(2435, 3098, 2446, 3081);
    private int value = 0;
    private Area tut = Area.rectangular(3044, 3143, 3177, 3046);
    private Area cave = Area.rectangular(3070, 9533, 3118, 9494);
    private Area tut2 = Area.rectangular(1637, 6148, 1758, 6062);
    private Area cave2 = Area.rectangular(1662, 12541, 1710, 12497);
    public static String muleName = "MuleName";
    public static int minValue = 2000;
    private Map<Integer, Integer> itemsMap = new HashMap<>();
    private boolean mapped = false;
    private File looterDir = new File(Script.getDataDirectory() + "/acclooter/");
    private File keepItems = new File(Script.getDataDirectory() + "/itemfilter.txt");
    private List<String> restrictedItems = Arrays.asList("weed", "poop");
    public static boolean gui = true;
    private Area duel = Area.rectangular(3309, 3287, 3408, 3196);
    private List<String> keepItemsList = new ArrayList<>();
    private boolean teleHome = false;

    public void onStart() {
        removeBlockingEvent(LoginScreen.class);
        PriceHelper.fetchPrices();
        GameCanvas.setInputEnabled(true);
        looterDir.mkdirs();
        SwingUtilities.invokeLater(Gui::new);

        if (!Files.exists(txtPath)) {
            try {
                new File(txtPath.toUri()).createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!Files.exists(out)) {
            try {
                new File(out.toUri()).createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!keepItems.exists()) {
            try {
                keepItems.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            Files.readAllLines(txtPath).forEach((acc) -> {
                String[] accSplit = acc.split(":");
                if (accSplit.length > 1) {
                    loadedAccs.push(new Acc(accSplit[0], accSplit[1]));
                } else {
                    System.out.println("Line: " + acc + " is wrongly formatted");
                }

            });

            Files.readAllLines(out).forEach((acc) -> {
                String[] accSplit = acc.split(":");
                Acc checkedAcc = null;
                if (accSplit.length > 5) {
                    checkedAcc = new Acc(accSplit[0], accSplit[1], accSplit[2].replace("VALUE=", ""), accSplit[3].replace("COMBAT_LVL=", ""), accSplit[4].replace("TOTAL_LVL=", ""), accSplit[5].replace("MEMBERSHIP=", ""));
                }

                if (accSplit.length > 6) {
                    String skillsText = accSplit[6].replace("{", "").replace("}", "").replace(" ", "").replace("SKILLS=", "");
                    String[] skillsArray = skillsText.split(",");
                    Map<String, Integer> skillsMap = new HashMap<>();

                    for (String skillText : skillsArray) {
                        String[] skillEntry = skillText.split("=");
                        String skillName = skillEntry[0];
                        int skillLvl = Integer.parseInt(skillEntry[1]);
                        skillsMap.put(skillName, skillLvl);
                    }

                    checkedAcc.setSkills(skillsMap);
                }

                checkedAccs.add(checkedAcc);
            });
        } catch (IOException var2) {
            var2.printStackTrace();
        }

        Collections.shuffle(loadedAccs);
    }

    public int loop() {
        try {
            keepItemsList.clear();
            keepItemsList.addAll(Files.readAllLines(keepItems.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        InterfaceComponent minigamesTab = Interfaces.getComponent(76, 0);
        InterfaceComponent[] minigamesTabButton = Interfaces.get(548, (x -> x.containsAction("Kourend tasks") || x.containsAction("Minigames") || x.containsAction("Quest list") || x.containsAction("Adventure paths")));
        if (acc != null && checkedAccs.stream().anyMatch((x) -> x.getUsername().equals(acc.getUsername()))) {
            Log.info("already checked this account: " + acc);
            acc = loadedAccs.pop();
            account = new GameAccount(acc.getUsername(), acc.getPassword());
            return 150;
        }

        if (gui) {
            Log.info("gui window");
            return 1000;
        }

        if (acc == null && loadedAccs.size() == 0) {
            Log.info("loaded accs size = 0, stopping");
            return -1;
        }

        if (account != null && acc != null) {
            if (done(acc)) {
                if (response != null) {
                    if (response.contains("authenticator")) {
                        acc.setBankValue("AUTHENTICATOR");
                    }

                    if (response.contains("invalid")) {
                        acc.setBankValue("INVALID");
                    }

                    if (response.contains("disabled")) {
                        acc.setBankValue("BANNED");
                    }

                    if (response.contains("locked")) {
                        acc.setBankValue("LOCKED");
                    }

                    if (response.contains("iron")) {
                        acc.setBankValue("IRONMAN(" + acc.getBankValue() + ")");
                    }

                    if (response.contains("billing")) {
                        acc.setBankValue("BILLING_ISSUE");
                    }

                    if (response.contains("not logged out")) {
                        acc.setBankValue("ALREADY_LOGGED");
                    }

                    if (response.contains("restricted")) {
                        acc.setBankValue("RESTRICTED(" + acc.getBankValue() + ")");
                    }

                    if (response.contains("members-only area")) {
                        acc.setBankValue("MEMBERS_AREA");
                    }

                    response = null;
                }

                Time.sleep(1000);

                if (Game.isLoggedIn()) {
                    acc.setCombatLvl(String.valueOf(Players.getLocal().getCombatLevel()));
                    acc.setTotalLvl(String.valueOf(Skills.getTotalLevel()));
                    acc.setMembershipDays(String.valueOf(Game.getRemainingMembershipDays()));
                    Map<String, Integer> skillMap = new HashMap<>();

                    for (Skill skill : Skill.values()) {
                        skillMap.put(skill.toString(), Skills.getLevel(skill));
                    }

                    acc.setSkills(skillMap);
                    Game.logout();
                    Time.sleepUntil(Game::isOnCredentialsScreen, 10000L);
                } else {
                    acc.setCombatLvl(String.valueOf(0));
                    acc.setTotalLvl(String.valueOf(0));
                    acc.setMembershipDays(String.valueOf(0));
                }

                Log.info("checked: " + acc);
                checkedAccs.add(acc);
                itemsMap.clear();
                mapped = false;
                acc = null;
                account = null;
                value = 0;
                RSPeer.setGameAccount(null);

                try {
                    Files.write(out, checkedAccs.stream().map(Acc::toString).collect(Collectors.toList()), StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException var13) {
                    var13.printStackTrace();
                }

                return 1000;
            }

            if (RSPeer.getGameAccount() == null) {
                PriceHelper.fetchPrices();
                Log.info("setting gameaccount " + account);
                RSPeer.setGameAccount(account);
                return 50;
            }

            if (!Game.isLoggedIn() && account != null && account.validate() && Game.getState() != 25 && Game.getState() != 45) {
                if (Game.getState() == 20) {
                    return 300;
                }

                if (!Game.isLoggedIn()) {
                    if (Game.getClient().getLoginResponse1().contains("members") && !f2p.contains(Game.getClient().getCurrentWorld())) {
                        Log.info("setting f2p world");
                        Game.getClient().setWorld(Worlds.get((x) -> !x.isMembers() && !x.isBounty() && !x.isDeadman() && !x.isHighRisk() && !x.isPVP() && !x.isSeasonDeadman() && !x.isSkillTotal() && !x.isTournament() && !x.getActivity().toLowerCase().contains("pvp") && !x.getActivity().toLowerCase().contains("bounty")));
                        return 1000;
                    }

                    if (Game.getClient().isLoginWorldSelectorOpen()) {
                        Game.getClient().setLoginWorldSelectorOpen(false);
                        Log.info("close login selector");
                        return 1000;
                    }

                    Game.getClient().setUsername(acc.getUsername());
                    Game.getClient().setPassword(acc.getPassword());
                    System.out.println("logging in with: " + acc.getUsername() + ":" + acc.getPassword());
                    Game.getClient().setGameState(20);
                    return 1000;
                }

                return 3000;
            }

            if (Game.getClient().isInInstancedScene()) {
                Npc death = Npcs.getNearest("Death");
                SceneObject coffer = SceneObjects.getNearest("Death's coffer");

                if (death != null && coffer != null) {
                    if (Players.getLocal().isMoving()) {
                        return 1000;
                    }

                    if (Dialog.isViewingChatOptions()) {
                        List<InterfaceComponent> completedDialogs = Arrays.stream(Dialog.getChatOptions()).filter(x -> x.getText().contains("<str>")).collect(Collectors.toList());
                        if (completedDialogs.size() >= 4) {
                            SceneObjects.getNearest("Portal").interact("Use");
                            return 5000;
                        }

                        InterfaceComponent incompleteDialog = Arrays.stream(Dialog.getChatOptions()).filter(x -> !completedDialogs.contains(x)).findFirst().orElse(null);
                        if (incompleteDialog != null && incompleteDialog.isVisible()) {
                            Dialog.process(Arrays.asList(Dialog.getChatOptions()).indexOf(incompleteDialog));
                            return 1000;
                        }
                    }

                    if (!Dialog.canContinue()) {
                        death.interact("Talk-to");
                        return 1000;
                    }

                    Dialog.processContinue();
                    return 1000;
                }
            }

            if (Interfaces.getComponent(11, 2) != null && Interfaces.getComponent(11, 2).isVisible()) {
                Movement.setWalkFlag(Players.getLocal().getPosition().randomize(3));
                return 1000;
            }

            if (Varps.get(170) > 0) {
                InterfaceComponent displayName = Interfaces.getComponent(261, 71);
                displayName.interact("Toggle number of Mouse Buttons");
                Log.info("trying to disable mouse button setting");
                return 1000;
            }

            if (!tut.contains(Players.getLocal()) && !cave.contains(Players.getLocal()) && !tut2.contains(Players.getLocal()) && !cave2.contains(Players.getLocal())) {
                InterfaceComponent displayName = Interfaces.getComponent(558, 7);
                InterfaceComponent fixed;
                if (displayName != null && displayName.isVisible()) {
                    fixed = Interfaces.getComponent(558, 14);
                    InterfaceComponent setName = Interfaces.getComponent(558, 18);
                    if (setName != null && setName.isVisible() && setName.containsAction("Set name")) {
                        Log.info("setting name");
                        setName.interact("Set name");
                        return 500;
                    }

                    if (fixed != null && fixed.isVisible()) {
                        Log.info("choosing name");
                        fixed.interact("Set name");
                        return 500;
                    }

                    if (!EnterInput.isOpen()) {
                        Log.info("entering name");
                        displayName.interact(57);
                        return 500;
                    }

                    Log.info("typing name");
                    EnterInput.initiate("weed");
                    return 500;
                }

                InterfaceComponent chatboxInput = Interfaces.getComponent(162, 58);
                if (chatboxInput != null && chatboxInput.isVisible() && chatboxInput.containsAction("Configure")) {
                    if (Bank.isOpen()) {
                        Log.info("closing bank");
                        Bank.close();
                        return 1000;
                    }

                    Log.info("setting display name");
                    chatboxInput.interact("Configure");
                    return 1000;
                }

                if (Game.getClient().isResizableMode()) {
                    Log.info("setting fixed mode");
                    fixed = Interfaces.getComponent(261, 33);
                    if (fixed != null) {
                        fixed.interact("Fixed mode");
                        return 1000;
                    }
                }

                InterfaceComponent privateChatButton = Interfaces.getComponent(162, 18);
                InterfaceComponent privateChatText = Interfaces.getComponent(162, 22);
                if (privateChatButton != null && privateChatButton.isVisible()
                        && privateChatText != null && privateChatText.isVisible() && privateChatText.getText().contains("On")) {
                    privateChatButton.interact(x -> x.contains("Off"));
                    Log.info("Disable priv chat");
                    return 1000;
                }

                InterfaceComponent destroyDialog = Interfaces.getComponent(584, 0);
                if (destroyDialog != null && destroyDialog.isVisible()) {
                    Keyboard.sendKey('1');
                    Log.info("Processing destroy dialog");
                    return 1000;
                }

                Item payDirt = Inventory.getFirst(12011);
                if (payDirt != null) {
                    payDirt.interact("Drop");
                    Log.info("dropping paydirt");
                    return 500;
                }

                File muleFile = new File(Script.getDataDirectory() + "/acclooter/" + Players.getLocal().getName() + "%" + Worlds.getCurrent() + "%" + muleName);
                List<Integer> mems = Arrays.stream(Worlds.getLoaded(RSWorld::isMembers)).map(RSWorld::getId).collect(Collectors.toList());
                Item lamp = Inventory.getFirst((x) -> x.getName().toLowerCase().contains("lamp") || x.getName().toLowerCase().contains("knowledge"));
                if (lamp != null) {
                    if (lamp.containsAction("Destroy")) {
                        Log.info("destroying exp lamp/book");
                        lamp.interact("Destroy");
                    }

                    if (lamp.containsAction("Drop")) {
                        Log.info("dropping exp lamp/book");
                        lamp.interact("Drop");
                    }

                    return 650;
                }

                if (Players.getLocal().isAnimating()) {
                    return 1000;
                }

                if (duel.contains(Players.getLocal()) || teleHome) {
                    Log.info("we are at duel arena, going to lumb");
                    Magic.cast(Modern.HOME_TELEPORT);
                    teleHome = false;
                    return 3000;
                }

                if (Game.getRemainingMembershipDays() > 0 && !mems.contains(Worlds.getCurrent())) {
                    if (Dialog.canContinue() || Bank.isOpen()) {
                        Movement.setWalkFlag(Players.getLocal());
                        return 1000;
                    }

                    WorldHopper.randomHop((x) -> x.isMembers() && !x.isBounty() && !x.isDeadman() && !x.isHighRisk() && !x.isPVP() && !x.isSeasonDeadman() && !x.isSkillTotal() && !x.isTournament() && !x.getActivity().toLowerCase().contains("pvp") && !x.getActivity().toLowerCase().contains("bounty"));
                    return 5000;
                }

                if (ExWilderness.getLevel() > 0) {
                    acc.setBankValue("IN_WILDY");
                    Log.info("acc is in wildy, skipping");
                    return 1000;
                }

                if (!cw.contains(Players.getLocal())) {
                    if (!Players.getLocal().isAnimating()) {
                        if (minigamesTab != null && minigamesTab.isVisible()) {
                            InterfaceComponent membersComponent = Interfaces.getComponent(284, 5);
                            if (membersComponent != null && membersComponent.isVisible()) {
                                Movement.setWalkFlag(Players.getLocal().getPosition().translate(1, 0));
                                return 1000;
                            }

                            if (Dialog.isOpen()) {
                                Movement.setWalkFlag(Players.getLocal().getPosition().translate(1, 0));
                                return 1000;
                            }

                            Log.info("tele");
                            Minigames.teleport(Destination.CASTLE_WARS);
                            return 2000;
                        }

                        Log.info("open minigames");
                        if (minigamesTabButton.length > 0) {
                            minigamesTabButton[0].interact(x -> true);
                        } else {
                            Log.info("cant find minigames button, trying to force click");
                            Mouse.click(true, 606, 186);
                        }

                        Time.sleep(1000);
                        Interfaces.getComponent(629, 13).interact(x -> true);
                        return 1000;

                    }

                    return 333;
                }

                int invItemsSize;
                int var10;

                if (Game.getAccountType() == AccountType.ULTIMATE_IRONMAN) {
                    response = "iron";
                    return 500;
                }

                InterfaceComponent pendingPin = Interfaces.newQuery().texts(x -> x.contains("No, I didn't ask for this. Cancel it.")).results().first();
                if (pendingPin != null) {
                    pendingPin.click();
                    Log.info("stopping pending pin");
                    return 1000;
                }

                if (!mapped) {
                    if (!Bank.isOpen()) {
                        Bank.open();
                        Log.info("mapping bank: opening bank");
                        return 1000;
                    }

                    if (Interfaces.getComponent(664, 28) != null) {
                        Interfaces.getComponent(664, 28).click();
                        Time.sleepUntil(() -> Interfaces.getComponent(664, 28) == null, 300, 5000);
                    }

                    if (Varps.getBitValue(3755) > 0) {
                        InterfaceComponent placeholderToggle = Interfaces.getComponent(12, 37);
                        if (placeholderToggle != null && placeholderToggle.isVisible()) {
                            placeholderToggle.interact("Disable");
                        }

                        return 1000;
                    }

                    InterfaceComponent placeholders = Interfaces.getComponent(12, 55);
                    if (Bank.getFirst(x -> x.getStackSize() == 0) != null) {
                        if (placeholders != null) {
                            Log.info("mapping bank: removing placeholders");
                            placeholders.interact(1007, 5);
                        }

                        return 1000;
                    }

                    InterfaceComponent bankMain = Interfaces.getComponent(12, 10, 10);
                    if (!Bank.isMainTabOpen()) {
                        Log.info("mapping bank: opening main tab");
                        bankMain.interact(x -> true);
                        return 1000;
                    }

                    Item filler = Bank.getFirst(x -> x.containsAction("Clear-All"));
                    if (filler != null) {
                        Log.info("mapping bank: removing bank fillers");
                        filler.interact("Clear-All");
                        return 1000;
                    }


                    if (Varps.getBitValue(8352) > 0) {
                        InterfaceComponent depositInvSetting = Interfaces.getComponent(12, 54);
                        Log.info("mapping bank: enabling deposit inv button ");
                        depositInvSetting.interact("Show");
                        return 1000;
                    }

                    if (Varps.getBitValue(5364) > 0) {
                        InterfaceComponent depositInvSetting = Interfaces.getComponent(12, 53);
                        Log.info("mapping bank: enabling deposit equip button ");
                        depositInvSetting.interact("Show");
                        return 1000;
                    }

                    for (Item item : Inventory.getItems()) {
                        if (item.getId() == 995 && item.getStackSize() >= minValue) {
                            Log.info("mapping bank - inv: adding " + item.getName() + " value: " + item.getStackSize());
                            itemsMap.put(item.getId(), item.getStackSize());
                        }

                        if ((PriceHelper.ITEM_PRICES_ID.containsKey(item.getId())
                                && PriceHelper.ITEM_PRICES_ID.get(item.getId()).getSellPrice() * item.getStackSize() >= minValue)
                                || keepItemsList.contains(Definitions.getItem(item.getId()).getName())
                        ) {
                            ItemPrice price = PriceHelper.ITEM_PRICES_ID.get(item.getId());

                            if (Game.getRemainingMembershipDays() == 0 && (Definitions.getItem(item.getId()).isMembers() || Players.getLocal().getCombatLevel() <= 35 && restrictedItems.contains(Definitions.getItem(item.getId()).getName()))) {
                                Log.info("mapping bank - inv: can't loot p2p: " + item.getName() + " value: " + (price != null ? price.getSellPrice() * item.getStackSize() : -1));
                                value += (PriceHelper.ITEM_PRICES_ID.get(item.getId())).getSellPrice() * item.getStackSize();
                            } else {
                                Log.info("mapping bank - inv: adding " + item.getName() + " value: " + (price != null ? price.getSellPrice() * item.getStackSize() : -1));
                                itemsMap.put(item.getId(), item.getStackSize());
                            }
                        }
                    }

                    for (Item item : Equipment.getItems()) {
                        if ((PriceHelper.ITEM_PRICES_ID.containsKey(item.getId())
                                && PriceHelper.ITEM_PRICES_ID.get(item.getId()).getSellPrice() * item.getStackSize() >= minValue)
                                || keepItemsList.contains(Definitions.getItem(item.getId()).getName())
                        ) {
                            ItemPrice price = PriceHelper.ITEM_PRICES_ID.get(item.getId());

                            if (Game.getRemainingMembershipDays() == 0 && (Definitions.getItem(item.getId()).isMembers() || Players.getLocal().getCombatLevel() <= 35 && restrictedItems.contains(Definitions.getItem(item.getId()).getName()))) {
                                Log.info("mapping bank - equips: can't loot p2p: " + item.getName() + " value: " + (price != null ? price.getSellPrice() * item.getStackSize() : -1));
                                value += (PriceHelper.ITEM_PRICES_ID.get(item.getId())).getSellPrice() * item.getStackSize();
                            } else {
                                itemsMap.put(item.getId(), item.getStackSize());
                                Log.info("mapping bank - equips: adding " + item.getName() + " value: " + (price != null ? price.getSellPrice() * item.getStackSize() : -1));
                            }
                        }
                    }

                    for (Item bankitem : Bank.getItems()) {
                        if ((bankitem.getId() == 995 || bankitem.getId() == 13204) && bankitem.getStackSize() >= minValue) {
                            Log.info("mapping bank - bankinv: adding " + bankitem.getName() + " value: " + bankitem.getStackSize());
                            itemsMap.put(bankitem.getId(), bankitem.getStackSize());
                        }

                        ItemPrice price = PriceHelper.ITEM_PRICES_ID.get(bankitem.getId());

                        if ((PriceHelper.ITEM_PRICES_ID.containsKey(bankitem.getId())
                                && PriceHelper.ITEM_PRICES_ID.get(bankitem.getId()).getSellPrice() * bankitem.getStackSize() >= minValue)
                                || keepItemsList.contains(Definitions.getItem(bankitem.getId()).getName())
                        ) {
                            if (Game.getRemainingMembershipDays() == 0 && (Definitions.getItem(bankitem.getId()).isMembers() || Players.getLocal().getCombatLevel() <= 35 && restrictedItems.contains(Definitions.getItem(bankitem.getId()).getName()))) {
                                Log.info("mapping bank - bankinv: can't loot p2p: " + bankitem.getName() + " value: " + (price != null ? price.getSellPrice() * bankitem.getStackSize() : -1));
                                value += (PriceHelper.ITEM_PRICES_ID.get(bankitem.getId())).getSellPrice() * bankitem.getStackSize();
                            } else {
                                itemsMap.put(bankitem.getId(), bankitem.getStackSize());
                                Log.info("mapping bank - bankinv: adding " + bankitem.getName() + " value: " + (price != null ? price.getSellPrice() * bankitem.getStackSize() : -1));
                            }
                        }
                    }

                    if (getFreeSpace() < Equipment.getItems().length + Inventory.getItems().length) {
                        acc.setBankValue("BANK_FULL(" + NumericFormat.apply(value) + ")");
                        return 1000;
                    }

                    if (getFreeSpace() > Equipment.getItems().length + Inventory.getItems().length) {
                        if (Inventory.getItems().length > 0) {
                            Log.info("mapping bank: deposit inv");
                            Bank.depositInventory();
                            return 1000;
                        }

                        if (Equipment.getItems().length > 0) {
                            Log.info("mapping bank: deposit equips");
                            Bank.depositEquipment();
                            return 1000;
                        }
                    }

                    mapped = true;
                    return 1000;
                }

                if (itemsMap.isEmpty() && Inventory.isEmpty() && !Trade.isOpen()) {
                    for (File file : Objects.requireNonNull(looterDir.listFiles())) {
                        if (!file.isDirectory()) {
                            Log.info("items map empty, del " + file.getName());
                            file.delete();
                        }
                    }

                    acc.setBankValue(NumericFormat.apply(value));
                    Log.fine("acc: " + acc + "leftover value: " + acc.getBankValue());
                    return 1000;
                }

                if (Game.getAccountType() != AccountType.NORMAL) {
                    response = "iron";
                    return 100;
                }

                if (Inventory.isFull() || Trade.isOpen() || !Inventory.isEmpty() && itemsMap.isEmpty()) {
                    if (looterDir.listFiles() != null) {
                        for (File file : Objects.requireNonNull(looterDir.listFiles())) {
                            if (file.getName().contains(Players.getLocal().getName()) && !file.getName().contains(String.valueOf(Worlds.getCurrent()))) {
                                Log.info("hopped, replace " + file.getName());
                                file.delete();
                            }
                        }
                    }

                    if (!muleFile.exists()) {
                        try {
                            Log.info("create new " + muleFile.getName());
                            muleFile.createNewFile();
                        } catch (IOException var14) {
                            var14.printStackTrace();
                        }
                    }

                    if (!Trade.isOpen()) {
                        Player mule = Players.getNearest(muleName);
                        if (mule != null) {
                            Log.info("muling - trading mule");
                            mule.interact("Trade with");
                            return 3000;
                        }

                        return 1000;
                    }

                    if (Trade.isOpen(true)) {
                        Trade.accept();
                        Log.info("trade - accepting trade");
                        return 1000;
                    }

                    if (Inventory.isEmpty()) {
                        Log.info("muling - accepting trade");
                        Trade.accept();
                        return 1000;
                    }

                    for (Item inventoryItem : Inventory.getItems()) {
                        if (Inventory.isEmpty()) {
                            Log.info("trade - accepting trade");
                            break;
                        }

                        if (!inventoryItem.isExchangeable() && inventoryItem.getId() != 995 && inventoryItem.getId() != 13204) {
                            if (inventoryItem.containsAction("Destroy")) {
                                Log.info("destroying " + inventoryItem.getName());
                                inventoryItem.interact("Destroy");
                            } else {
                                Log.info("dropping " + inventoryItem.getName());
                                inventoryItem.interact("Drop");
                            }

                            return 750;
                        }

                        if (response != null && response.equals("restricted")) {
                            Log.info("account is restricted, stopping trade");
                            return 1000;
                        }

                        Log.info("muling - offering: " + inventoryItem.getName());
                        Trade.offerAll(inventoryItem.getId());
                        Time.sleep(350, 550);
                    }

                    return 350;
                }

                if (Inventory.getItems().length - Equipment.getItems().length > 0) {
                    for (Item inventoryItem : Inventory.getItems()) {
                        if (Inventory.isFull()) {
                            break;
                        }

                        Log.info("unequipping " + inventoryItem.getName());
                        inventoryItem.interact("Remove");
                        Time.sleep(350, 550);
                    }

                    return 1000;
                }

                if (!Bank.isOpen()) {
                    Log.info("going to open bank");
                    Bank.open();
                    Time.sleepUntil(Bank::isOpen, 5000L);
                    return 1000;
                }

                if (Bank.getWithdrawMode() == WithdrawMode.ITEM) {
                    Log.info("setting withdraw mode to noted");
                    Bank.setWithdrawMode(WithdrawMode.NOTE);
                    return 1000;
                }

                for (Item bankItem : Bank.getItems()) {
                    int id = bankItem.getId();
                    Log.info(itemsMap.toString());
                    if (!itemsMap.containsKey(id)) {
                        Log.info("don't need to withdraw " + bankItem.getName());
                    } else {
                        if (Inventory.isFull()) {
                            Log.info("stopped withdrawing, inv full");
                            break;
                        }

                        Log.info("withdrawing " + bankItem.getName() + " " + bankItem.getStackSize() + "x");
                        Bank.withdrawAll(id);
                        Time.sleep(650, 850);
                        itemsMap.remove(id);
                    }
                }

                return 600;
            }

            acc.setBankValue("TUTORIAL_ISLAND");
            return 1000;
        }

        acc = loadedAccs.pop();
        account = new GameAccount(acc.getUsername(), acc.getPassword());
        Log.info(loadedAccs.size() + " accs to go");
        return 600;
    }

    private boolean done(Acc acc) {
        InterfaceComponent pinWindow = Interfaces.getComponent(213, 0);

        if (pinWindow != null && pinWindow.isVisible()) {
            InterfaceComponent deletedText = Interfaces.getComponent(213, 2);
            if (deletedText != null && deletedText.isVisible()) {
                String[] textSplit = deletedText.getText().split(" ");
                if (textSplit.length > 6) {
                    int days = Integer.parseInt(textSplit[6]);
                    acc.setBankValue("BANKPIN(" + days + ")");
                    return true;
                }
            }

            InterfaceComponent forgotBtn = Interfaces.getComponent(213, 15);
            if (forgotBtn != null && forgotBtn.isVisible()) {
                forgotBtn.interact("I don't know it.");
            }

            return false;
        }

        return acc.getBankValue() != null || response != null && (response.contains("iron") || response.contains("locked") || response.contains("disabled") || response.contains("invalid") || response.contains("authenticator") || response.contains("billing") || response.contains("not logged out") || response.contains("restricted") || response.contains("members-only area"));
    }

    private int getFreeSpace() {
        InterfaceComponent bankCount = Interfaces.getComponent(12, 5);
        InterfaceComponent bankCap = Interfaces.getComponent(12, 8);
        return Integer.parseInt(bankCap.getText()) - Integer.parseInt(bankCount.getText());
    }

    public void notify(LoginResponseEvent e) {
        if (Game.isLoggedIn()) {
            response = null;
        } else {
            response = e.getResponse().getMessage().toLowerCase();
        }
    }

    public void notify(ChatMessageEvent e) {
        if (e.getMessage().toLowerCase().contains("you are an iron ") || e.getMessage().contains("ultimate iron man")) {
            response = "iron";
        }

        if (e.getMessage().toLowerCase().contains("restricted")) {
            response = "restricted";
        }

        if (e.getMessage().toLowerCase().contains("minigame teleports")) {
            teleHome = true;
        }
    }
}
