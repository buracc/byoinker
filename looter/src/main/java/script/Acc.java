//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

import java.util.Map;

public class Acc {
    private String username;
    private String password;
    private String bankValue;
    private String combatLvl;
    private String totalLvl;
    private String membershipDays;
    private Map<String, Integer> skills;

    public Acc(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Acc(String username, String password, String bankValue, String combatLvl, String totalLvl, String membershipDays) {
        this.username = username;
        this.password = password;
        this.bankValue = bankValue;
        this.combatLvl = combatLvl;
        this.totalLvl = totalLvl;
        this.membershipDays = membershipDays;
    }

    public Acc(String username, String password, String bankValue, String combatLvl, String totalLvl, String membershipDays, Map<String, Integer> skills) {
        this.username = username;
        this.password = password;
        this.bankValue = bankValue;
        this.combatLvl = combatLvl;
        this.totalLvl = totalLvl;
        this.membershipDays = membershipDays;
        this.skills = skills;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBankValue() {
        return this.bankValue;
    }

    public void setBankValue(String bankValue) {
        this.bankValue = bankValue;
    }

    public String getCombatLvl() {
        return this.combatLvl;
    }

    public void setCombatLvl(String combatLvl) {
        this.combatLvl = combatLvl;
    }

    public String getTotalLvl() {
        return this.totalLvl;
    }

    public void setTotalLvl(String totalLvl) {
        this.totalLvl = totalLvl;
    }

    public String getMembershipDays() {
        return this.membershipDays;
    }

    public void setMembershipDays(String membershipDays) {
        this.membershipDays = membershipDays;
    }

    public Map<String, Integer> getSkills() {
        return this.skills;
    }

    public void setSkills(Map<String, Integer> skills) {
        this.skills = skills;
    }

    public String toString() {
        return this.username + ":" + this.password + ":VALUE=" + this.bankValue + ":COMBAT_LVL=" + this.combatLvl + ":TOTAL_LVL=" + this.totalLvl + ":MEMBERSHIP=" + this.membershipDays + (this.skills != null ? ":SKILLS=" + this.skills.toString() : "");
    }
}
