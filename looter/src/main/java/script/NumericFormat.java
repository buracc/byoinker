//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

public final class NumericFormat {
    public static final byte COMMAS = 1;
    public static final byte THOUSANDS = 64;
    private static final char[] POSTFIXES = new char[]{'K', 'M', 'B', 'T'};
    private static final char HYPHEN = '-';
    private static final char COMMA = ',';
    private static final char ZERO = '0';
    private static final char DOT = '.';

    private NumericFormat() {
        throw new IllegalAccessError();
    }

    public static int precision(int precision) {
        return precision << 2;
    }

    public static String apply(long value) {
        return apply(value, 65 | precision(2));
    }

    public static String apply(long value, int settings) {
        StringBuilder builder = new StringBuilder(32);
        builder.append(value);
        char[] buff = builder.toString().toCharArray();
        boolean commas = (settings & 1) == 1;
        int precision = 0;
        int postfix = 0;
        if (settings >= 64) {
            postfix = settings >> 6;
            if (postfix > POSTFIXES.length) {
                postfix = POSTFIXES.length;
            }
        }

        if (settings > 1) {
            precision = settings >> 2 & 15;
        }

        builder.setLength(0);
        int negative = 0;
        if (buff[0] == '-') {
            negative = 1;
        }

        int length = buff.length - negative;
        if (postfix * 3 >= length) {
            postfix = (int)((double)length * 0.334D);
            if (postfix * 3 == length && precision == 0) {
                --postfix;
            }
        }

        int end = length - postfix * 3;
        int start = length % 3;
        if (start == 0) {
            start = 3;
        }

        start += negative;
        if (end > 0 && negative == 1) {
            builder.append('-');
        }

        int max = end + negative;

        int i;
        for(i = negative; i < max; ++i) {
            if (i == start && i + 2 < max && commas) {
                start += 3;
                builder.append(',');
            }

            builder.append(buff[i]);
        }

        if (postfix > 0) {
            if (end == 0) {
                if (negative == 1 && precision > 0) {
                    builder.append('-');
                }

                builder.append('0');
            }

            max = precision + end + negative;
            if (max > buff.length) {
                max = buff.length;
            }

            for(end += negative; max > end && buff[max - 1] == '0'; --max) {
            }

            if (max - end != 0) {
                builder.append('.');
            }

            for(i = end; i < max; ++i) {
                builder.append(buff[i]);
            }

            builder.append(POSTFIXES[postfix - 1]);
        }

        return builder.toString();
    }
}
