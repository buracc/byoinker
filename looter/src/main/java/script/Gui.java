//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import org.rspeer.runetek.api.Game;

public class Gui extends JFrame {
    private final JTextField muleName;
    private final JSpinner value;

    public Gui() {
        this.setLayout(new GridLayout(3, 2));
        this.setDefaultCloseOperation(2);
        this.getInsets().set(10, 10, 10, 10);
        this.setResizable(false);
        this.muleName = new JTextField(Looter.muleName);
        this.value = new JSpinner(new SpinnerNumberModel(Looter.minValue, 0, 2147483647, 1));
        JButton start = new JButton("Start");
        this.add(new JLabel("Mule name:"));
        this.add(this.muleName);
        this.add(new JLabel());
        this.add(new JLabel("Min. value: "));
        this.add(this.value);
        this.add(new JLabel());
        this.add(start);
        start.addActionListener((e) -> {
            this.start();
        });
        this.pack();
        this.setLocationRelativeTo(Game.getCanvas());
        this.setVisible(true);
    }

    private void start() {
        Looter.gui = false;
        Looter.muleName = this.muleName.getText();
        Looter.minValue = (Integer)this.value.getValue();
        this.dispose();
    }
}
