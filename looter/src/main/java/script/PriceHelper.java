//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.rspeer.runetek.api.commons.Time;

public class PriceHelper {
    public static final Map<String, ItemPrice> ITEM_PRICES = new HashMap();
    public static final Map<Integer, ItemPrice> ITEM_PRICES_ID = new HashMap();

    public PriceHelper() {
    }

    public static void fetchPrices() {
        try {
            String urlString = "https://rsbuddy.com/exchange/summary.json";
            String data = sendGET(urlString, 4);
            if (data == null) {
                throw new Exception("Failed to get item prices.");
            }

            JSONObject apiObj = new JSONObject(data);
            apiObj.keySet().forEach((key) -> {
                JSONObject item = (JSONObject)apiObj.get(key);
                String name = item.getString("name");
                int id = item.getInt("id");
                int buyPrice = item.getInt("buy_average");
                int sellPrice = item.getInt("sell_average");
                int overallPrice = item.getInt("overall_average");
                ItemPrice price = new ItemPrice(id, name, buyPrice, sellPrice, overallPrice);
                ITEM_PRICES.put(item.getString("name"), price);
                ITEM_PRICES_ID.put(item.getInt("id"), price);
            });
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    private static String sendGET(String getUrl, int retriesLeft) throws IOException {
        URL obj = new URL(getUrl);
        HttpURLConnection con = (HttpURLConnection)obj.openConnection();
        int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            if (retriesLeft >= 1) {
                Time.sleep(200);
                return sendGET(getUrl, retriesLeft - 1);
            } else {
                return null;
            }
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder response = new StringBuilder();

            String inputLine;
            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            return response.toString();
        }
    }
}
